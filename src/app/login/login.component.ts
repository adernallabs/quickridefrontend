import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { URLSearchParams } from '@angular/http';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    urlSearchParams;
    loginForm: FormGroup;
    countryCode:string;
    userId;

    constructor(public router: Router, public loginService: LoginService) { }

    ngOnInit() {
        this.urlSearchParams = new URLSearchParams();
        this.loginForm = new FormGroup({
            phone: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
        });
        this.countryCode = "%2B91";
    }

    loginUser() {
        this.urlSearchParams = new URLSearchParams();
        this.urlSearchParams.append('userId', +this.loginForm.value.phone);
        this.urlSearchParams.append('pwd', this.loginForm.value.password);
     //   this.urlSearchParams.append('countryCode',this.countryCode );
        this.urlSearchParams.append('appName', 'Quick Ride');
        this.loginService.login(this.urlSearchParams)
            .subscribe(
                (data) => {
                    console.log(data.json());

                    localStorage.setItem('isLoggedin', 'true');
                    localStorage.setItem('userId', data.json().resultData.phone);
                    this.userId = data.json().resultData.phone;
                    //this.getRiderDetails();

                    if (this.checkIfNewUser()) {
                        this.router.navigateByUrl('/ride-profile');
                    }
                    else {
                        this.router.navigateByUrl('/dashboard');
                    }


                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                    console.log(error);
                }
            );

    }

    checkIfNewUser() {
        return JSON.parse(localStorage.getItem("newUser"));
    }

    //Phone number validation
    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

}
