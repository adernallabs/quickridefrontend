import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../../router.animations';

import { FormGroup , FormControl , Validators } from '@angular/forms';
import { RegisterVehicleService } from './register-vehicle.service';
import { URLSearchParams } from '@angular/http';
import { ShowVehiclesComponent } from './show-vehicles/show-vehicles.component';

@Component({
  selector: 'app-register-vehicle',
  templateUrl: './register-vehicle.component.html',
  styleUrls: ['./register-vehicle.component.scss'],
  animations: [routerTransition()]
})
export class RegisterVehicleComponent implements OnInit {

  userId:any;

  showBikeForm: boolean;
  showCarForm: boolean;

  urlSearchParams: any;
  totalSeats=[];
  carForm:FormGroup;
  bikeForm:FormGroup;
  showVehicle:boolean;
  vehicle={};

  @ViewChild(ShowVehiclesComponent)
  private vehicleComponent : ShowVehiclesComponent;

  constructor(public registerVehicleService:RegisterVehicleService) { }

  ngOnInit() {
    this.showVehicle=false;
    this.setTotalCarSeats();
    this.urlSearchParams = new URLSearchParams();
    this.showCarForm=true;
    this.showBikeForm=false;
    this.userId = localStorage.getItem("userId");
    console.log(this.userId);
  //  this.getVehicle();

    this.carForm = new FormGroup({
      model: new FormControl('',Validators.required),
      registrationNo : new FormControl('',Validators.required),
      fare : new FormControl(3,Validators.required),
      capacity : new FormControl(4,Validators.required),
      features : new FormControl('',Validators.required),
      category : new FormControl('',Validators.required),
    });

    this.bikeForm = new FormGroup({
      model: new FormControl('',Validators.required),
      registrationNo : new FormControl('',Validators.required),
      fare : new FormControl(3,Validators.required),
      capacity : new FormControl(1,Validators.required),
      features : new FormControl('',Validators.required),
      category : new FormControl('',Validators.required),
    });
  }

  setTotalCarSeats(){
    for(var i=1;i<=6;i++){
      this.totalSeats[i-1] = i;
    }
  }
  //Submit Car Form
  registerCar(){
    //alert("Hi");
    console.log(this.carForm.value);
    if(this.carForm.value.model==''||
      this.carForm.value.registrationNo==''||
      this.carForm.value.capacity=='' ||
      this.carForm.value.fare==''
      ){
        alert("Please fill all fields !");
      }
      else{
        this.urlSearchParams = new URLSearchParams();
        this.urlSearchParams.append('ownerid',this.userId.toString());
        this.urlSearchParams.append('model',this.carForm.value.model);
        this.urlSearchParams.append('regno',this.carForm.value.registrationNo);
        this.urlSearchParams.append('capacity',this.carForm.value.capacity);
        this.urlSearchParams.append('fare',this.carForm.value.fare);
        this.urlSearchParams.append('vehicleType','Car');
        this.urlSearchParams.append('makeAndCategory',this.carForm.value.category);
        this.registerVehicleService.registerVehicle(this.urlSearchParams)
        .subscribe(
          (data) => {
          console.log(data.json());
          alert("Added Car !");
      },
      (error) => {
        alert(error.json().resultData.userMsg);
        console.log(error);
      }
    );
      }

  }

  //Register Bike Form
  registerBike(){
    //alert("Hi");
    console.log(this.bikeForm.value);
    if(this.bikeForm.value.model==''||
      this.bikeForm.value.registrationNo==''||
      this.bikeForm.value.capacity=='' ||
      this.bikeForm.value.fare==''
      ){
        alert("Please fill all fields !");
      }
      else{
        this.urlSearchParams = new URLSearchParams();
        this.urlSearchParams.append('model',this.bikeForm.value.model);
        this.urlSearchParams.append('regno',this.bikeForm.value.registrationNo);
        this.urlSearchParams.append('ownerid',this.userId.toString());
        this.urlSearchParams.append('capacity',this.bikeForm.value.capacity);
        this.urlSearchParams.append('fare',this.bikeForm.value.fare);
        this.urlSearchParams.append('vehicleType','Bike');
        this.urlSearchParams.append('makeAndCategory',this.bikeForm.value.category);

        this.registerVehicleService.registerVehicle(this.urlSearchParams)
          .subscribe(
            (data) => {
            console.log(data.json());
            alert("Added Bike");

      // localStorage.setItem('isLoggedin', 'true');
      // this.router.navigateByUrl('/dashboard');
        //this.router.navigateByUrl('/dashboard');

        },
        (error) => {
          alert(error.json().resultData.userMsg);
          console.log(error)
        }
        );
      }
  }


//Toggle between forms
  enableCarForm(){
    this.showCarForm=true;
    this.showBikeForm=false;
  }
  enableBikeForm(){
    this.showCarForm = false;
    this.showBikeForm=true;


  }

  showVehicles(){
    this.showVehicle = true;
  }
  //Toggle between tabs
  onSelectTabChange(event){
    if(event.index==2){
      this.vehicleComponent.getVehicles();
    }
  }
}
