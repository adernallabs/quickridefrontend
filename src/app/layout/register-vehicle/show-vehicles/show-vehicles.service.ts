import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class ShowVehicleService {
    temp: {};
    constructor(private http: Http) { }


    getVehicle(Credentials) {
        const headers = new Headers({});
       //console.log(Credentials);
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRUser/completeProfile/multivehicle',{headers:headers,params:Credentials});
    }

    deleteVehicle(Credentials){
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRVehicle/update',Credentials);
    }


}