import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class EditVehicleService {
    temp: {};
    constructor(private http: Http) { }


    editVehicle(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRVehicle/update',Credentials,{headers:headers});
    }

}