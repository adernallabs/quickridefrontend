import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class RegisterVehicleService {
    temp: {};
    constructor(private http: Http) { }

    registerVehicle(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.post('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRVehicle',Credentials,{headers:headers});
    }




}