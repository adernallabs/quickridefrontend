
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class RideService {
    temp: {};
    constructor(private http: Http) { }

    getRideRoute(routeId) {
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRoutePath/routePathData?routeId='+routeId);
    }


}
