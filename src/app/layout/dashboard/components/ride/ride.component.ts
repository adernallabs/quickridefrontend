import { Component, OnInit, ChangeDetectorRef, ViewChild, Output, EventEmitter, Input, AfterViewInit } from '@angular/core';
import { DirectionsRenderer } from '@ngui/map';
import { RideService } from './ride.service';
import { Router } from '@angular/router';



declare var require: any;
declare var google: any;


@Component({
  selector: 'app-ride',
  templateUrl: './ride.component.html',
  styleUrls: ['./ride.component.scss']
})
export class RideComponent implements OnInit {
  rideRoute: any;
  //time variables
  startTime: string;
  mm: any;
  HH: any;
  yyyy: string;
  MM: any;
  dd: any;
  currentTime: Date;
  //
//Get details from dashboard component
  @Input() ridePosted;
  @Input() rideStart;
  @Input() defaultVehicle;
//Send data to dashboard component
  @Output()
  pickupCoords = new EventEmitter<string>();

  @Output()
  destinationCoords = new EventEmitter<string>();

  @Output()
  pickupAddress = new EventEmitter<string>();

  @Output()
  rideTime = new EventEmitter<string>();

  @Output()
  Address: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  callPostRide = new EventEmitter<string>();

  @Output()
  callEditRide = new EventEmitter<string>();


  @Output()
  rideRouteObject = new EventEmitter<string>();



  @Output()
  destinationAddress = new EventEmitter<string>();



  @ViewChild(DirectionsRenderer) directionsRendererDirective: DirectionsRenderer;

  typingPickupLocation:boolean;
  typingDestinationLocation:boolean;
  showMarker: boolean;
  showDirections: boolean;
  endAddress: string;
  startAddress: string;

  allOptions: {};
  latlng: { lat: number; lng: number; };
  lng: number;
  lat: number;
  destLat: number;
  destLng: number;
  destLatLng: { lat: number, lng: number };
  location: Coordinates;
  positions: any;
  autocomplete: any;
  address: any = {};
  directionsRenderer: google.maps.DirectionsRenderer;
  directionsResult: google.maps.DirectionsResult;
  direction = {};

  currentRide;

  editedStartAddress: string;
  editedEndAddress: string;
  editedLatLng: { lat: number; lng: number; };
  editedDestLatLng: { lat: number; lng: number; };
  editedRideTime: string;

  vehicleText: string;
  constructor(private ref: ChangeDetectorRef, private cdr: ChangeDetectorRef, private rideService: RideService,public router:Router) {

  }


  ngOnInit() {
    this.typingPickupLocation=false;
    this.typingDestinationLocation=false;
    this.editedStartAddress = null;
    this.editedEndAddress = null;
    this.editedDestLatLng = null;
    this.editedLatLng = null;
    this.editedRideTime = null;
    this.getCurrentTime();
    this.vehicleText = this.defaultVehicle.model + " " + this.defaultVehicle.regno;
  //  console.log(this.vehicleText);
    //console.log(this.defaultVehicle);
 //Check if active ride is going on or not
    this.currentRide = JSON.parse(localStorage.getItem("CurrentRide"));

    if (this.currentRide !== null) {
      this.startAddress = this.currentRide.startAddress;
      this.endAddress = this.currentRide.endAddress;
      this.startTime = (new Date(this.currentRide.startTime)).toString();
      this.lat = this.currentRide.startLatitude;
      this.lng = this.currentRide.startLongitude;
      this.destLat = this.currentRide.endLatitude;
      this.destLng = this.currentRide.endLongitude;
      this.latlng = {
        lat: this.lat,
        lng: this.lng
      }
      this.destLatLng = {
        lat: this.destLat,
        lng: this.destLng
      }
    }
    this.ridePosted = JSON.parse(localStorage.getItem("RidePosted"));


  }
  togglePickupLocation(){
    console.log("tpl");
    this.typingPickupLocation=!this.typingPickupLocation;

  }
  toggleDestinationLocation(){
    console.log("tdl");
    this.typingDestinationLocation=!this.typingDestinationLocation;

  }
  onMapReady(map) {
   // console.log('map', map);
   // console.log('markers', map.markers);  // to get all markers as an array
  }
  onIdle(event) {
    console.log('map', event.target);
  }
  onMarkerInit(marker) {
    console.log('marker', marker);
  }
  onMapClick(event) {
    this.positions.push(event.latLng);
    event.target.panTo(event.latLng);
  }
  initialized(autocomplete: any) {
    this.autocomplete = autocomplete;
  }
  directionsChanged() {
    this.directionsResult = this.directionsRenderer.getDirections();
    this.cdr.detectChanges();
  }

  showDirection() {
    this.directionsRendererDirective['showDirections'](this.direction);
  }
  initialiseDirection() {
    this.directionsRendererDirective['initialized$'].subscribe(directionsRenderer => {
      this.directionsRenderer = directionsRenderer;
    });
  }

  //Google places autcomplete
  autocompleteDestination(place) {
    this.destLat = place.geometry.location.lat();
    this.destLng = place.geometry.location.lng();
    this.destLatLng = {
      lat: this.destLat,
      lng: this.destLng
    };
    this.editedDestLatLng = this.destLatLng;
    //localStorage.setItem("destLatLng",JSON.stringify(this.destLatLng));

    for (let i = 0; i < place.address_components.length; i++) {
      let addressType = place.address_components[i].types[0];
      this.address[addressType] = place.address_components[i].long_name;

    }


    this.ref.detectChanges();
    this.endAddress = place.formatted_address;
   // this.toggleDestinationLocation();


    this.editedEndAddress = place.formatted_address;

    this.destinationAddress.emit(this.endAddress);
    this.destinationCoords.emit(JSON.stringify(this.destLatLng));










  }
  //Google places autocomplete
  autocompletePickup(place) {
    this.lat = place.geometry.location.lat();
    this.lng = place.geometry.location.lng();
    this.latlng = {
      lat: this.lat,
      lng: this.lng
    };

    this.editedLatLng = this.latlng;
    //console.log("Lat lng :"+this.latlng);
    //  localStorage.setItem("latlng",JSON.stringify(this.latlng));

    for (let i = 0; i < place.address_components.length; i++) {
      let addressType = place.address_components[i].types[0];
      this.address[addressType] = place.address_components[i].long_name;

    }


    this.ref.detectChanges();
    this.startAddress = place.formatted_address;
    //this.togglePickupLocation();


    this.editedStartAddress = place.formatted_address;


    this.pickupAddress.emit(this.startAddress);
    this.pickupCoords.emit(JSON.stringify(this.latlng));





  }
  public getCurrentTime() {
    this.currentTime = new Date();

    this.getStartTime();

  }
  getStartTime() {
    this.dd = this.addTrailingZeroToDate(this.currentTime.getDate());
    this.MM = this.addTrailingZeroToDate(this.currentTime.getMonth() + 1);
    this.yyyy = this.currentTime.getFullYear().toString();
    this.HH = this.addTrailingZeroToDate(this.currentTime.getHours());
    this.mm = this.addTrailingZeroToDate(this.currentTime.getMinutes());

    this.startTime = (this.dd + this.MM + this.yyyy + this.HH + this.mm);

    this.dd = this.addTrailingZeroToDate(this.currentTime.getUTCDate());
    this.MM = this.addTrailingZeroToDate(this.currentTime.getUTCMonth() + 1);
    this.yyyy = this.currentTime.getUTCFullYear().toString();
    this.HH = this.addTrailingZeroToDate(this.currentTime.getUTCHours());
    this.mm = this.addTrailingZeroToDate(this.currentTime.getUTCMinutes());

    this.editedRideTime = (this.dd + this.MM + this.yyyy + this.HH + this.mm);




  }

  addTrailingZeroToDate(unit) {
    unit = unit > 9 ? unit : '0' + unit;
    return unit.toString();

  }

  //Emit all data to dashboard component
  postRide() {
    this.pickupAddress.emit(this.startAddress);
    this.pickupCoords.emit(JSON.stringify(this.latlng));
    this.destinationAddress.emit(this.endAddress);
    this.destinationCoords.emit(JSON.stringify(this.destLatLng));
    this.rideTime.emit(this.startTime);
    this.callPostRide.emit("true");


  }
  getRideRoute() {

    this.rideService.getRideRoute(this.currentRide.routeId)
      .subscribe(
        (response) => {
          this.rideRoute = response.json().resultData;
         // console.log(this.rideRoute);
          this.rideRouteObject.emit(JSON.stringify(this.rideRoute));
          this.callEditRide.emit("true");
        },
        (error) => {
          alert(error.json().resultData.userMsg);
          console.log(error);
        }
      );

  }
  //Emit all data to dashboard component
  editRide() {
    this.getRideRoute();
    this.pickupAddress.emit(this.editedStartAddress);
    this.pickupCoords.emit(JSON.stringify(this.editedLatLng));
    this.destinationAddress.emit(this.editedEndAddress);
    this.destinationCoords.emit(JSON.stringify(this.editedDestLatLng));
    this.destinationAddress.emit(this.editedEndAddress);
    this.rideTime.emit(this.editedRideTime);


  }
  showAllVehicles(){
    this.router.navigateByUrl("/show-vehicles");
  }
}
