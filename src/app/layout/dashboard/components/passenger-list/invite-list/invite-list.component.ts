import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { PassengerListService } from '../passenger-list.service';
import { Subscription } from 'rxjs/Subscription';
import { routerTransition } from '../../../../../router.animations';
import { MatSnackBar } from '@angular/material';
import { URLSearchParams } from '@angular/http';

@Component({
  selector: 'app-invite-list',
  templateUrl: './invite-list.component.html',
  styleUrls: ['./invite-list.component.scss'],
  animations: [routerTransition()]
})
export class InviteListComponent implements OnInit {

  parameters:URLSearchParams;
  sub: Subscription;
  routeData: any;
  passengerList=[];
  currentRide;

  constructor(private route: ActivatedRoute,
    private router: Router, private passengerListService: PassengerListService,public snackBar: MatSnackBar) { }

  ngOnInit() {

    this.parameters = new URLSearchParams();
    this.route.params.subscribe((params) =>{
      this.passengerList = (JSON.parse(params.passengerList));
      this.currentRide =JSON.parse(params.currentRide);
      console.log(this.currentRide);
      console.log(this.passengerList);
    });
  }
  //Invite ride passenger
  invitePassenger() {
    this.parameters.append('id', this.currentRide.id);
    this.parameters.append('userId', this.currentRide.userId);
    this.parameters.append('MatchedUser', JSON.stringify(this.passengerList));
    this.passengerListService.invitePassengers(this.parameters)
      .subscribe(
        (response) => {
          this.openSnackBar('Invited', '', 3000);
          //After successful invitation , send user back to dashboard
          this.router.navigateByUrl('/dashboard');


        },
        (error) => {
          this.openSnackBar('Failed to invite', '', 3000);
          console.log(error);
        }
      );
  }
  openSnackBar(message: string, action: string, time) {
    this.snackBar.open(message, action, {
      duration: time,
    });
  }
}
