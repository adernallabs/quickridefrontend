
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class PassengerListService {
    temp: {};
    public inviteList:any;
    constructor(private http: Http) { }

    invitePassengers(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRidematcher/invite/passengers',Credentials,{headers:headers});
    }

    findMatchingRiders(Credentials) {
        const headers = new Headers({});
        //console.log(Credentials);
        return this.http.post('https://testrr.getquickride.com:8443/routematchserver/rest/QRRoutematcher/passengersWithOldMatches',Credentials,{headers:headers});
    }

    sendChat(Credentials){
        const headers= new Headers({});
        console.log(Credentials);
        return this.http.put("https://testrm.getquickride.com:8443/dishaapiserver/rest/QRConversation/conversationDeviceNotification",Credentials,{headers:headers});
    }
    getRideDetails(id){
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRide/participant/all?id='+id);
    }
    addPassenger(Credentials){
        console.log(Credentials);
        const headers= new Headers({});
        return this.http.put("https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRidematcher/joinrides",Credentials,{headers:headers});
    }

}