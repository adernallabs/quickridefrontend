import { Component, OnInit, Input , Output , EventEmitter } from '@angular/core';


@Component({
  selector: 'app-trip-report',
  templateUrl: './trip-report.component.html',
  styleUrls: ['./trip-report.component.scss']
})
export class TripReportComponent implements OnInit {

  currentRating: number;
  @Input() tripReport;

//Send data to dashboard component to show feedback page
  @Output()
  feedback = new EventEmitter<string>();

  fromLocation;
//Send data to dashboard component to show trip report page
  @Output()
  tripreport = new EventEmitter<string>();

  bill:any ;
  test='gg';
  rider={};
  constructor() { }

  ngOnInit() {

    this.currentRating=8;
    this.tripReport = JSON.parse(localStorage.getItem('tripReport'));
    this.bill  = this.tripReport.bills[0];
    //Check if there were any passengers .Show trip report page if true.
    if(this.bill===undefined){
      this.tripreport.emit(JSON.stringify('false'));
    }
    else{
      this.fromLocation = this.bill.startLocation;
      console.log(this.fromLocation);
    }
  }
  tripComplete() {
    this.rider = {
      riderId:this.bill.userId,
      name:this.bill.userName
    };
    //If trip report page is shown , show feedback page as well .
      if(this.bill!=undefined){
        this.feedback.emit(JSON.stringify(this.rider));
      }
      else{
        //
      }

    }

}
