import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class DashboardService {
    temp: {};
    constructor(private http: Http) { }

    getRiderDetails(Credentials) {
        const headers = new Headers({});
       //console.log(Credentials);
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRUser/completeProfile/multivehicle',{headers:headers,params:Credentials});
    }

    postRide(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.post('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRiderRide',Credentials,{headers:headers});
    }

    updateRide(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRiderRide/update/utc',Credentials,{headers:headers});
    }

    findMatchingRiders(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.post('http://13.126.226.56:8080/routematchserver/rest/QRRoutematcher/passengersWithOldMatches',
          Credentials,
          {headers:headers});
    }
    startRide(Credentials) {
        const headers = new Headers();
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRiderRide/status',Credentials,{headers:headers});
    }

    stopRide(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRiderRide/complete',Credentials,{headers:headers});
    }
    getAllRides(userId) {
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRide/all?userId='+userId);
    }
    getActiveRides(userId) {
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRide/activeride/rider?userId='+userId);
    }
    invitePassengers(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRidematcher/invite/passengers',Credentials,{headers:headers});
    }
    checkInvitations(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRegularridematcher/matchedpassenger',
        {headers:headers, params:Credentials});
    }
    cancelRide(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRRiderRide/cancelRide',Credentials,{headers:headers});
    }
    getDefaultVehicle(userId) {
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRVehicle?ownerid='+userId);
    }
    getTripReport(Credentials) {
        const headers =  new Headers({});
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRBilling/rider',{headers: headers, params:Credentials});
    }
}
