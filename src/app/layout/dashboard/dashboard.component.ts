import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, NgZone, Input, Directive, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { } from 'googlemaps';
import { } from '@types/googlemaps';
import { DashboardService } from './dashboard.service';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { Subscription } from 'rxjs/Subscription';
import { MatSnackBar } from '@angular/material';

import { Router } from '@angular/router';
import { Paho } from 'ng2-mqtt/mqttws31';
import { UUID } from 'angular2-uuid';
import { PassengerListComponent, MapComponent } from './components';





declare var require: any;
declare var google: any;


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})


export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {


    rideRoute: any;
    //  myOtherMessage$: Observable<MqttMessage>;
    @ViewChild("passenger") passenger : PassengerListComponent;
    client: Paho.MQTT.Client;
    host: string = "13.127.171.104"
    port: number = 61614;
    uuid = UUID.UUID();
    messages=[];
    userId: number;
    myMessage:any;

    // private client: Paho.MQTT.Client;

    profileName;
    rider: any;
    riderName: any;
    feedBackForm: boolean;
    riderId: any;
    feedback: any;
    tripReport: any;
    tripCompleted: boolean;

    showEditRideForm:boolean;



    defaultVehicle: any;
    invitationAccepted: boolean;
    allRideDetails: any;
    ActiveRides: any;
    AllRides: any;
    getCurrentPosition: Subscription;
    checkPassengerInvitations: Subscription;
    findPassengers: Subscription;

    rideStart: boolean;
    ridePosted: boolean;

    passengerList = [];
    showRideInfo: boolean; // Used to flag start-ride , ongoing rides

    // Time Variables
    startTime: string;
    mm: string;
    HH: string;
    yyyy: string;
    MM: string;
    dd: string;

    currentVehicle;

    destLng: number;
    destLat: number;
    urlSearchParams: any;
    rideId;

    latlng: { lat: number; lng: number; };
    destLatLong: { lat: number; lng: number; };

    lat: number;
    lng: number;
    currentAddress = '';
    currentTime;
    routeForm: FormGroup;
    showDirection: boolean;

    destinationAddress;
    currentRide;


@ViewChild(MapComponent) mapComponent:MapComponent;



    constructor(public dashboardService: DashboardService, public snackBar: MatSnackBar, public router: Router,
    ) {

    }


    ngOnInit() {
        this.showEditRideForm=false;


        this.defaultVehicle = {};
        this.invitationAccepted = false;
        this.showDirection = false;
        this.urlSearchParams = new URLSearchParams();
        this.getLocalData();
        this.getRiderDetails();
        this.getDefaultVehicle();
      //  this.getCurrentTime();
        this.getAllRides();
        this.destinationAddress=null;
        //  this.connectToTopic();
        if (this.currentRide) {
            this.lat = this.currentRide.startLatitude;
            this.lng = this.currentRide.startLongitude;
         //   console.log("Lat lng set from current ride");
            this.destLat = this.currentRide.endLatitude;
            this.destLng = this.currentRide.endLongitude;
            this.rideId = this.currentRide.id;
            console.log(this.rideId);

        }
        if ((this.rideStart) || (this.ridePosted)) {
            this.showRideInfo = true;
        } else {
            this.showRideInfo = false;
            this.rideStart = false;

        }


    }
    ngAfterViewInit() {
       // this.passenger.connectToMqtt();
    }
    ngOnDestroy() {

    }
    getRideRoute(event){
        this.rideRoute = event;
        if(this.latlng===null || this.destLatLong===null){
            this.rideRoute=null;
        }

    }
    //Get rider details and store them in RiderProfile for local use
    getRiderDetails = () => {
        this.urlSearchParams.append('userId', this.userId.toString());
        this.dashboardService.getRiderDetails(this.urlSearchParams)
        .subscribe(
            (response) => {
           //  console.log(response.json().resultData);
             localStorage.setItem("RiderProfile",JSON.stringify(response.json().resultData));
             this.profileName = response.json().resultData.user.name;
            },
            (error) => {
                alert(error.json().resultData.userMsg);
                console.log(error);
            }
        );


    }
    getLocalData() {
        this.userId = Number(localStorage.getItem('userId'));
      //  this.connectToMqtt();
        this.currentVehicle = localStorage.getItem('VehicleList');
        this.currentRide = JSON.parse(localStorage.getItem('CurrentRide'));
        this.passengerList = JSON.parse(localStorage.getItem('passengers'));
        this.rideStart = JSON.parse(localStorage.getItem('RideStatus'));
        this.ridePosted = JSON.parse(localStorage.getItem('RidePosted'));
        this.feedBackForm = JSON.parse(localStorage.getItem('feedBackForm'));

    }
    //Fetching pickup coordinates from map or autocomplete function
    public getPickupCoords(event) {
        this.latlng = JSON.parse(event);
        if(this.latlng!==null){
            this.lat = this.latlng.lat;
        this.lng = this.latlng.lng;
       // this.mapComponent.resetStrokeWeight();
     //   console.log("Lat lng :"+this.latlng);
        }
        else{
            this.lat =null;
            this.lng = null;

        }

    }
    //Fetching destination coordinates from map or autocomplete function
    public getDestinationCoords(event) {
        this.destLatLong = JSON.parse(event);
        if(this.destLatLong!==null){
            this.destLat = this.destLatLong.lat;
        this.destLng = this.destLatLong.lng;
     //   this.mapComponent.resetStrokeWeight();
        }
        else{
            this.destLat=null;
            this.destLng=null;
            this.rideRoute=null;
        }

    }
    //Fetching pickup address from map or autcomplete function
    public getPickupAddress(event) {
        this.currentAddress = event;
      //  console.log("Pickup Address :" + this.currentAddress);

    }
    //Fetching destination address from map or autocomplete function
    public getDestinationAddress(event) {
        this.destinationAddress = event;
      //  console.log("Dest Address :" + this.destinationAddress);
    }
    //Fetching ride time from ride component
    public getRideTime(event){
        this.startTime = event;
     //   console.log(this.startTime);

    }
    // Post Ride as a Rider
    postRide() {
        //  this.connectToTopic();
      //  console.log("Lat :"+this.lat);
      //  console.log("lng :"+this.lng);
     // this.currentAddress=(<HTMLInputElement>document.getElementById('pickupAddress')).value
       if(this.destinationAddress===undefined || this.destinationAddress===null || this.currentAddress===null || this.startTime===null){
           alert("Please enter all fields !");
       }
       else{
           this.urlSearchParams = new URLSearchParams();
           console.log(this.currentAddress);
        this.urlSearchParams.append('userId',this.userId.toString());
        this.urlSearchParams.append('startAddress', this.currentAddress);
        this.urlSearchParams.append('startLatitude', this.lat);
        this.urlSearchParams.append('startLongitude', this.lng);
        this.urlSearchParams.append('endAddress', this.destinationAddress);
        this.urlSearchParams.append('endLatitude', this.destLat);
        this.urlSearchParams.append('endLongitude', this.destLng);
        this.urlSearchParams.append('startTime', this.startTime);
        this.urlSearchParams.append('vehicleNumber', this.defaultVehicle['regno']);
        this.urlSearchParams.append('vehicleModel', this.defaultVehicle['model']);
        this.urlSearchParams.append('availableSeats', this.defaultVehicle['capacity']);
        this.urlSearchParams.append('farePerKm', this.defaultVehicle['fare']);
        this.urlSearchParams.append('capacity', this.defaultVehicle['capacity']);
        this.urlSearchParams.append('vehicleType', this.defaultVehicle['vehicleType']);

        this.dashboardService.postRide(this.urlSearchParams)
            .subscribe(
                (response) => {
                  //  console.log(response.json());
                    this.rideId = response.json().resultData.id;
                   // console.log('Ride Id: ' + this.rideId);
                    this.currentRide = response.json().resultData;
                    localStorage.setItem('CurrentRide', JSON.stringify(this.currentRide));
                    this.getActiveRides();
                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                    console.log(error);
                }
            );

       }

    }

    updateRide(){

         this.urlSearchParams = new URLSearchParams();

         this.urlSearchParams.append('startAddress', this.currentAddress);
         this.urlSearchParams.append('startLatitude', this.lat);
         this.urlSearchParams.append('startLongitude', this.lng);
         this.urlSearchParams.append('endAddress', this.destinationAddress);
         this.urlSearchParams.append('endLatitude', this.destLat);
         this.urlSearchParams.append('endLongitude', this.destLng);
         this.urlSearchParams.append('startTime', this.startTime);
         this.urlSearchParams.append('vehicleNumber',null);
         this.urlSearchParams.append('vehicleModel', null);
         this.urlSearchParams.append('availableSeats', null);
         this.urlSearchParams.append('farePerKm', null);
         this.urlSearchParams.append('capacity', null);
         this.urlSearchParams.append('routeId', this.currentRide.routeId);
         this.urlSearchParams.append('vehicleId', null);
         this.urlSearchParams.append('capacity', null);
         this.urlSearchParams.append('id',this.currentRide.id);
         this.urlSearchParams.append('route', this.rideRoute);
         this.urlSearchParams.append('makeAndCategory', null);
         this.urlSearchParams.append('vehicleType', null);

         this.dashboardService.updateRide(this.urlSearchParams)
             .subscribe(
                 (response) => {
                   //  console.log(response.json());
                     this.rideId = response.json().resultData.id;
                    // console.log('Ride Id: ' + this.rideId);
                     this.currentRide = response.json().resultData;
                     localStorage.setItem('CurrentRide', JSON.stringify(this.currentRide));
                     this.getActiveRides();
                 },
                 (error) => {
                    alert(error.json().resultData.userMsg);
                     console.log(error);
                 }
             );


    }
    editRide(){
        this.showEditRideForm=true;
        this.showRideInfo = !this.showRideInfo;
    }
    public getCurrentTime() {
        this.currentTime = new Date();

        this.getStartTime();
    }
    //Start time needs to be in the format ddMMyyyyHHmm
    getStartTime() {
      this.dd = this.addTrailingZeroToDate(this.currentTime.getDate());
      this.MM = this.addTrailingZeroToDate(this.currentTime.getMonth() + 1);
      this.yyyy = this.currentTime.getFullYear().toString();
      this.HH = this.addTrailingZeroToDate(this.currentTime.getHours());
      this.mm = this.addTrailingZeroToDate(this.currentTime.getMinutes());
      this.startTime = (this.dd + this.MM + this.yyyy + this.HH + this.mm);

    }
    //Add extra zeros to convert 6 to 06 or 9 to 09
    addTrailingZeroToDate(unit) {
      unit = unit > 9 ? unit : '0' + unit;
      return unit.toString();

    }

    changeMapStyles() {
        this.showRideInfo = !this.showRideInfo;
    }

    //Start ride
    startRide() {
        this.urlSearchParams.append('id', this.rideId);
        this.urlSearchParams.append('status', 'Started');
        this.dashboardService.startRide(this.urlSearchParams)
            .subscribe(
                (response) => {
                    //console.log(response.json());
                    this.rideStart = true;
                    localStorage.setItem('RideStatus', 'true');
                    this.openSnackBar('Ride Started', '');
                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                }
            );
    }
    //Stop ride
    stopRide() {
        this.urlSearchParams.append('id', this.rideId);
        this.dashboardService.stopRide(this.urlSearchParams)
            .subscribe(
                (response) => {
                    //console.log(response.json());
                    this.rideStart = false;
                    localStorage.setItem('RideStatus', 'false');
                    this.showRideInfo = false;
                    this.ridePosted = false;
                    localStorage.setItem('RidePosted', 'false');
                    this.openSnackBar('Ride Stopped', '');
                    localStorage.removeItem("CurrentRide");

                    this.showDirection = false;
                    this.mapComponent.refreshMap();
                    // this.latlng = { lat: null, lng: null };
                    // this.destLatLong = { lat: null, lng: null };
                    this.getTripReport();
                    this.ngOnInit();
                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                }
            );
    }
    cancelRide() {
        this.urlSearchParams.append('id', this.rideId);
        this.urlSearchParams.append('userId', this.userId);
        this.dashboardService.cancelRide(this.urlSearchParams)
            .subscribe(
                (response) => {
                    //console.log(response.json());
                    this.rideStart = false;
                    localStorage.setItem('RideStatus', 'false');
                    this.showRideInfo = false;
                    this.ridePosted = false;
                    this.showDirection = false;
                    this.mapComponent.refreshMap();

                    localStorage.removeItem("CurrentRide");
                    localStorage.setItem('RidePosted', 'false');
                    this.openSnackBar('Ride Cancelled !', '');

                    this.ngOnInit();
                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                }
            );
    }

    //Show snacbar messages
    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 3000,
        });
    }
    checkInvitees(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 5000,
        });
    }
    //Get information about all rides
    getAllRides() {
        this.dashboardService.getAllRides(this.userId.toString())
            .subscribe(
                (response) => {
                    this.AllRides = (response.json().resultData);
                    localStorage.setItem('AllRides', JSON.stringify(this.AllRides));
                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                }
            );

    }
    //Get active ride details. Essential to call this function when dashboard component is loaded.
    getActiveRides() {
        this.dashboardService.getActiveRides(this.userId.toString())
            .subscribe(
                (response) => {
                    //console.log(response.json());
                    this.ActiveRides = (response.json().resultData);
                    this.currentRide = this.ActiveRides[this.ActiveRides.length - 1];
                    localStorage.setItem('ActiveRides', JSON.stringify(this.ActiveRides));
                    this.showRideInfo = true;
                    this.ridePosted = true;
                    localStorage.setItem('RidePosted', 'true');

                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                }
            );

    }
    //Get default vehicle details
    getDefaultVehicle() {
        this.dashboardService.getDefaultVehicle(this.userId.toString())
            .subscribe(
                (data) => {

                    this.defaultVehicle = (data.json().resultData);
                 //   console.log(this.defaultVehicle);

                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                });

    }
    //Get trip report details
    getTripReport() {
       // this.getCurrentTime();
        this.urlSearchParams.append('id', this.currentRide.id);
        this.urlSearchParams.append('actualEndTime', this.startTime);
        this.urlSearchParams.append('startAddress', this.currentRide.startAddres);
        this.urlSearchParams.append('endAddress', this.currentRide.endAddress);
        this.urlSearchParams.append('userId', this.currentRide.userId);
        this.dashboardService.getTripReport(this.urlSearchParams)
            .subscribe(
                (data) => {
                    this.tripReport = (data.json().resultData);
                    localStorage.setItem('tripReport', JSON.stringify(this.tripReport));
                  //  console.log(this.tripReport);
                    if (this.tripReport.bills.length != 0) {
                        this.tripCompleted = true;
                        localStorage.setItem('tripCompleted', 'true');
                    }

                },
                (error) => {
                    alert(error.json().resultData.userMsg);
                });
    }

    //Show feedback form
    showFeedback(event) {

        this.feedBackForm = true;
        localStorage.setItem("feedBackForm", 'true');
        this.rider = JSON.parse(event);
        this.riderId = this.rider.riderId;
        this.riderName = this.rider.name;
        console.log(this.riderId);
    }


    showNewRide(event) {
        this.feedBackForm = false;
        this.tripCompleted = false;
    }
    showNewRide2(event) {
        this.feedBackForm = false;
        this.tripCompleted = false;
    }

}
