import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class VerifyService {
    temp: {};
    constructor(private http: Http) { }

    updateUser(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRUserProfile/updateIncludingGender/new',Credentials,{headers:headers});
    }
    getGender(Credentials) {
        const headers = new Headers({});
       //console.log(Credentials);
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRUser/gender',{headers:headers,params:Credentials});
    }



}