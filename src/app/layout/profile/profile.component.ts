import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profile;
  riderRides;
  passengerRides;
  totalRides;
  constructor() { }

  // Get Rider details
  ngOnInit() {
     this.profile = JSON.parse(localStorage.getItem("RiderProfile"));
     this.riderRides = this.profile.userProfile.noofridesasrider;
     this.passengerRides = this.profile.userProfile.noofridesaspassenger;
     ;
     this.totalRides = this.riderRides+this.passengerRides;
  }

}
