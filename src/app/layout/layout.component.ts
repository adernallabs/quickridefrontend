import { Component, OnInit } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { LayoutService } from './layout.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    urlSearchParams:URLSearchParams;
    userId;
    profileName;
    constructor(private layoutService:LayoutService) {}

    ngOnInit() {
        this.userId = (localStorage.getItem("userId"));
        this.getRiderDetails();
    }
    //Get rider detajls and send data to sidebar component
    getRiderDetails(){
       // console.log("Layout component");
        this.urlSearchParams = new URLSearchParams();
        this.urlSearchParams.append('userId', this.userId.toString());
        this.layoutService.getRiderDetails(this.urlSearchParams)
        .subscribe(
            (response) => {
             console.log("RIder details"+response.json().resultData);
           //  localStorage.setItem("RiderProfile",JSON.stringify(response.json().resultData));
             this.profileName = response.json().resultData.user.name;
             console.log(this.profileName);
            },
            (error) => {
              //  alert(error.json().resultData.userMsg);
                console.log(error);
            }
        );

    }
}
