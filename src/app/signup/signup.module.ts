import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SignupService } from './signup.service';
import {MatTooltipModule} from '@angular/material/tooltip';
import { SharedService } from '../shared/shared.service';


@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
    ReactiveFormsModule,
    MatTooltipModule
  ],
  providers:[SignupService,SharedService],
  declarations: [SignupComponent]
})
export class SignupModule { }
