import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { FormGroup , FormControl , Validators } from '@angular/forms';
import { SignupService } from './signup.service';
import { URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { UUID } from 'angular2-uuid';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    signupForm:FormGroup;
    signUpDetails;
    urlSearchParams;
    phone:string;
    name:string;
    countryCode:string;
    constructor(private signupService : SignupService,public router: Router) {}

    ngOnInit() {
        this.countryCode="%2B91";
        this.signupForm = new FormGroup({
            phone : new FormControl('',Validators.required),
            password : new FormControl('',Validators.required),
            name : new FormControl('',Validators.required),
            email : new FormControl('',Validators.required),
            gender : new FormControl('',Validators.required)
        });

        this.urlSearchParams = new URLSearchParams();
    }
    //Signup Form submission
    onSubmit(){
       // console.log(this.countryCode);
        let uuid = UUID.UUID();
       // console.log(this.signupForm.value);
        this.name = this.signupForm.value.name;
        this.phone = this.signupForm.value.phone;
        localStorage.setItem("name",this.name);
        localStorage.setItem("phone",this.phone);

        this.urlSearchParams.append('phone',this.signupForm.value.phone);
        this.urlSearchParams.append('pwd',this.signupForm.value.password);
        this.urlSearchParams.append('name',this.signupForm.value.name);
        this.urlSearchParams.append('email',this.signupForm.value.email);
        this.urlSearchParams.append('gender',this.signupForm.value.gender);
        this.urlSearchParams.append('status',null);
        this.urlSearchParams.append('referralCode',null);
        //this.urlSearchParams.append('countryCode',this.countryCode);
        this.urlSearchParams.append('androidAppVersionName',"6.54");
        this.urlSearchParams.append('appName','Quick Ride');
        this.urlSearchParams.append('deviceUniqueId',uuid);

        this.signupService.checkSignup(this.urlSearchParams)
        .subscribe(
        (response) => {
         console.log(response.json());
         localStorage.setItem("userName",this.signupForm.value.name);
         this.router.navigateByUrl('/activate-account');

        },
        (error) => {
          alert(error.json().resultData.userMsg);
          console.log(error.json().resultData.userMsg)
        }
        );
    }
    //Phone number validation
    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
          event.preventDefault();
        }
      }


}
