import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { SharedService } from '../shared/shared.service';

@Injectable()
export class SignupService {
    temp: {};
    constructor(private http: Http,private service:SharedService) { }

    checkSignup(Credentials) {
        const headers = new Headers();
        console.log(Credentials);
        return this.http.post(this.service.dishaapiserver+'/rest/QRUser',Credentials,
        { headers: headers });
    }



}