import { Component, OnInit } from '@angular/core';
import { FormGroup , FormControl , Validators } from '@angular/forms';
import { ActivateAccountService } from './activate-account.service';
import { URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.scss'],
  animations: [routerTransition()]
})
export class ActivateAccountComponent implements OnInit {

  phone: any;
  urlSearchParams;
  activateAccountForm:FormGroup;

  constructor(public router:Router, public activateAccountService:ActivateAccountService,private snackBar:MatSnackBar) {

   }

  ngOnInit() {
    this.urlSearchParams= new URLSearchParams();
    this.activateAccountForm = new FormGroup({
      activationCode : new FormControl('',Validators.required)
    });

    this.phone = JSON.parse(localStorage.getItem("phone"));

  }
//Activate User Account
  activate(){
    console.log(this.activateAccountForm.value);
    this.urlSearchParams = new URLSearchParams();
 //   this.urlSearchParams= new URLSearchParams();
    this.urlSearchParams.append('userId',this.phone);
    this.urlSearchParams.append('activationCode',this.activateAccountForm.value.activationCode.toString());
   // this.urlSearchParams.append('countryCode',"%2B91");
    this.urlSearchParams.append('appName','Quick Ride');
    this.activateAccountService.checkActivation(this.urlSearchParams)
        .subscribe(
        (data) => {
         console.log(data.json());
         localStorage.setItem("newUser","true");
         this.router.navigateByUrl('/login');

        },
        (error) => {
          alert(error.json().resultData.userMsg);
          console.log(error)
        }
        );

  }
  //Resend OTP
  resendCode(){
    this.urlSearchParams= new URLSearchParams();
    this.urlSearchParams.append('phone',this.phone.toString());
   // this.urlSearchParams.append('countryCode',"%2B91");
    this.urlSearchParams.append('appName','Quick Ride');
    this.activateAccountService.resendActivationCode(this.urlSearchParams)
        .subscribe(
        (data) => {
         console.log(data.json());
//         this.router.navigateByUrl('/dashboard');
          this.openSnackBar("Activation code sent to your phone !",'',3000);
        },
        (error) => {
          alert(error.json().resultData.userMsg);
          console.log(error)
        }
        );

  }
  openSnackBar(message: string, action: string, time) {
    this.snackBar.open(message, action, {
      duration: time,
    });
  }

}
