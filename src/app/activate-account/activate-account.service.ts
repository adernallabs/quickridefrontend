import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class ActivateAccountService {
    temp: {};
    constructor(private http: Http) { }

    checkActivation(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.get('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRUser/activateAccount',{headers:headers , params : Credentials},
        );
    }
    resendActivationCode(Credentials) {
        const headers = new Headers({});
        console.log(Credentials);
        return this.http.put('https://testrm.getquickride.com:8443/dishaapiserver/rest/QRUser/activationCode',Credentials
        );
    }


}